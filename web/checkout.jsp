<%-- 
    Document   : checkout
    Created on : 11-Feb-2014, 2:01:38 AM
    Author     : aeRO Gaming
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Bean.Item"%>
<jsp:useBean id='itemsOrder' class='Bean.Product' scope='session' />

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Confirmation</title>
    </head>
    <body>
        
        <form name="form1" action="order.html" method="post">
            
        <table border="1" style="width:300px">
        <tr>
        <th colspan="3">Order Confirmation</th>
        </tr>
        <% if(session.getAttribute("itemlist") == null){%>
        <p> You haven't had any order </p>
        <%} else {
            Map<Integer, Item> itemList = (HashMap)session.getAttribute("itemlist");
            double sum = 0;
            for(Map.Entry<Integer, Item> item : itemList.entrySet()){
                out.println("<tr><td>" + item.getValue().name + "</td>");
                out.println("<td><input type=\"number\" name=\""+item.getValue().getName()+"---"+item.getValue().price +"\" value=\""+item.getValue().getQuantity()+"\" min=\"1\" max=\"100\"></td>");
                out.println("<td>$" + item.getValue().getTotalPrice() + "</td></tr>");
                sum += item.getValue().getTotalPrice();
            }
            out.println("<input type=\"hidden\" name=\"totalPrice\" value=\""+sum+"\">");
            out.println("<td colspan=\"2\">Total Price</td>" + "<td>"+String.format("$%.2f", sum)+"</td></tr>");
            
            /*
            ArrayList<Item> name = (ArrayList<Item>) session.getAttribute("orderList");
            Map<Integer, Item> shoppingCart = (HashMap)session.getAttribute("itemlist");
            
            while(itr.hasNext()){
                Item item = itr.next();
                out.println(item.name);
            }
            */
        }
        %>
        </table>
        <span class="error">${message}</span>
            <input name="submit" type="submit" value="Refresh" />
            <input name="submit" type="submit" value="FinalCheckOut" />
            <input name="submit" type="submit" value="AddMoreItems" />
            
        </form>
    </body>
</html>
