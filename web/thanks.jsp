<%-- 
    Document   : thanks.jsp
    Created on : 15-Feb-2014, 10:41:15 PM
    Author     : aeRO Gaming
--%>

<jsp:useBean id='itemsOrder' class='Bean.Product' scope='session' />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Completed</title>
    </head>
    <body>
        <% 
            out.println("Thank you " + session.getAttribute("userid"));
        %>
        <h3>Your order has been successfully added. Thank you for your order.</h3>
    </body>
</html>
