<%-- 
    Document   : index.jsp
    Created on : 5-Feb-2014, 8:39:16 PM
    Author     : Adrian Roy Antonio Baguio
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id='items' class='Bean.Product' scope='page' />

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ordering System</title>
    </head>
    <body>

      <h1>Ordering System</h1>
      <form name="form1" action="order.html" method="post">
          <% if(session.getAttribute("userid")==null){ %>
          <p>User ID:<input type="text" name="userId"> </p>
          <% } else{ %>
          <p>User ID:<input type="text" name="userId" value="<%= session.getAttribute("userid") %>"> </p>
          <% } %>
          <p>Menu: </p>
        <select name="itemlist" multiple="multiple" id = "itemID" style="height:120px">
        <!-- 
            @author Adrian Roy A Baguio
            @date 11/02/14
            @description: Passed an actual object of items, and iterate through an array, and print their id name and price.
        -->
        <c:forEach var="getItems" items="${items.items}">
            <option value="<c:out value="${getItems.id}" />" name="<c:out value="${getItems.name}" />"><c:out value="${getItems.name}"/>  <c:out value="${getItems.price}"/> </option>
            <c:out value="${getItems.name}"/>
        </c:forEach>
        </select>
         
          
          <br><br>
        <input name="submit" type="submit" value="Reset" />
        <input name="submit" type="submit" value="CheckOut" />
        <br/><span class="error">${message}</span>
      </form>
    </body>
</html>
