/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

/**
 *
 * @author aeRO Gaming
 */
public class Product {
    
    // Variable declaration
    private Connection connection;
    private Statement stmt;
    ArrayList<Item> items = new ArrayList<Item>();
    
    
    /*
    Find the item base on ID.
    */
    public Item getItem(int id){
        for(Item item: items){
            if(item.id == id){
                return item;
            }
        }
        return null;
    
    }
    
    // Constructor
    public Product() { 
    }
    
    // Get cartItems
    public ArrayList<Item> getItemOrder(){
        return items;        
    }
    
    // Get items
    public ArrayList<Item> getItems() {
        
        try {
        // load the driver
        //Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        Class.forName("oracle.jdbc.OracleDriver").newInstance();
        //Get a Connection to the database          		
        connection = DriverManager.getConnection("jdbc:oracle:thin:@dilbert.humber.ca:1521:grok ", "YOURUSERNAME", "PASSWORD");

        System.out.println("Connection successful!");
        }  catch( Exception e ) { 
        // problem loading driver, class not exist?
         e.printStackTrace();
        }
        
        ResultSet rs = null;
            try {
            String query = "SELECT * from BGDR0015.ITEMS";
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                String name = rs.getString("NAME");
                String price = rs.getString("PRICE");
                String itemId = rs.getString("ITEM_ID");
                items.add(new Item(name, Double.parseDouble(price), Integer.parseInt(itemId)));
            }
                return items;
            } catch (SQLException ex) {
                Logger.getLogger(OrderServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        return null;
    }
    
    // Make database connection in servlet
    public void init (ServletConfig config) throws ServletException {

    try {
        // load the driver
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        //Get a Connection to the database          		
        connection = DriverManager.getConnection("jdbc:oracle:thin:@dilbert.humber.ca:1521:grok ", "YOURUSERNAME", "PASSWORD");

        System.out.println("Connection successful!");
    }  catch( Exception e ) { 
         // problem loading driver, class not exist?
             e.printStackTrace();
            return;
    }
    }

}
