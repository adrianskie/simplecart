package Bean;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Bean.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;

/**
 *
 * @author aeRO Gaming
 */
@WebServlet(urlPatterns = {"/OrderServlet"})
public class OrderServlet extends HttpServlet {
    
    // Variable declaration0
    
    private Connection connection;
    private Statement stmt;
    private ServletContext gc;
    Product p;
    
    // so there will be a unique item.
    private Map<Integer, Item> shoppingCart = new HashMap<Integer, Item>(); // <id, name>
    ArrayList<Item> items = new ArrayList<Item>();
    ArrayList<String> addedItems= new ArrayList<String>();


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            items = getItems();
            request.setAttribute("itemList", items);
            request.getRequestDispatcher("/index.jsp").forward(request, response);
            /*
            // If it doesn't exsist make a new one.
            if(request.getAttributeNames("itemlist") == null) {
                shoppingCart = new HashMap<Integer, Item>();   
            }
            // Check the shopping cart
            else{ 
                shoppingCart = request.getParameterValues("itemlist");
            }
            */
            

		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

     protected void setQuantity(String name, int quantity){
         for (Map.Entry<Integer, Item> entry : shoppingCart.entrySet()) {
            String itemName = entry.getValue().getName();
            // Check if it matches the name, then set the quantity.
            if(itemName.equals(name)){
                entry.getValue().setQuantity(quantity); // Set the quantity of the item.
                break;
            }
        }
     }
    
    protected void addItem(HttpServletRequest request){
        
        String id;
        Product p = new Product();
        p.getItems();
        HttpSession session = request.getSession();  // Make a session  
                   
        for(int x = 0; x < request.getParameterValues("itemlist").length; x++){
            id = request.getParameterValues("itemlist")[x].toString();

            if(!shoppingCart.containsKey(Integer.parseInt(id))) { // Check if its in shopping cart
                // Find for the object
                Item item = p.getItem(Integer.parseInt(id));
                shoppingCart.put(Integer.parseInt(id), item);
            }
        }
        session.setAttribute("itemlist", shoppingCart); // Add the shopping cart to cart session
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet -specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       //request.getAttributeNames("itemlist");
        
        switch(request.getParameter("submit")){
            case "FinalCheckOut":
                putItems(request);
                request.getRequestDispatcher("/thanks.jsp").forward(request, response);
                break;
            case "Refresh":
                Enumeration<String> param = request.getParameterNames();
                while(param.hasMoreElements()){
                    String name = (String) param.nextElement();
                    if(name.equals("submit") || name.equals("totalPrice")){continue;} // Last item
                    int quantity = Integer.parseInt(request.getParameter(name));
                    String[] parts = name.split("---");
                    String idname = parts[0];
                    setQuantity(idname, quantity); // Set the quantity of the item
                }
                request.getSession().setAttribute("itemlist", shoppingCart); // Update the shopping cart session.
                
                request.getRequestDispatcher("/checkout.jsp").forward(request, response); // now redirect them into checkout page
                break;
            case "AddMoreItems":
                
             break;
            // Case for reset
            case "Reset":
                request.setAttribute("message", "Your just reset your order.");
                
                request.getSession().removeAttribute("itemlist");
                request.getSession().invalidate();
                
                request.getSession();

                // Simply redirect to the index page
                //response.sendRedirect("/index.jsp");
                request.getRequestDispatcher("/index.jsp").forward(request, response);
                
                break;
            case "CheckOut":
                    // Check if the userID is blanked.
                    if(request.getParameter("userId").equals("")){
                        // This one is now working just uncomment it.
                        request.setAttribute("message", "Please type in your user id.");
                        break;
                     }
                    else{
                       // Set up a session
                       request.getSession().setAttribute("userid", request.getParameter("userId")); // Add the shopping cart to cart session
                    }
                    // If there are no item selected
                    if(request.getParameterValues("itemlist") == null){
                       request.setAttribute("message", "Please select an item.");
                        break;
                    }
                
                    // Check if there is a session
                    // There is no current session
                    if(request.getSession().getAttribute("itemlist") == null){
                        //request.getParameterValues('itemlist');
                        
                        shoppingCart = new HashMap<Integer, Item>(); // Reset the cart as well!
                        HttpSession session = request.getSession();  // Make a session
                        addItem(request);                            // Add item base on the request of user. 
                        
                    }
                    else{
                       addItem(request); // Assume that cart has been initialised already.
                       request.getRequestDispatcher("/checkout.jsp").forward(request, response); // now redirect them into checkout page
                    }
                 
                 request.setAttribute("message", "Checkout completly");
                 request.getRequestDispatcher("/checkout.jsp").forward(request, response);
                 break;
            default:
                request.setAttribute("message", "Something went wrong. Please contact the site administrator");
                break; 
        }

        
        // Selected Order
        //String[] foo = request.getParameterValues("itemlist");
        
        // Send an error message
        //request.setAttribute("error", "Unknown user, please try again");
        
        // Forward back to index.jsp
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        
        //processRequest(request, response);
     }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void init (ServletConfig config) throws ServletException {

 try {
 // load the driver
  //Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
     Class.forName("oracle.jdbc.OracleDriver").newInstance();
	//Get a Connection to the database          		
connection = DriverManager.getConnection("jdbc:oracle:thin:@dilbert.humber.ca:1521:grok ", "YOURUSERNAME", "PASSWORD");
           	
            System.out.println("Connection successful!");
        }  catch( Exception e ) { 
             // problem loading driver, class not exist?
         	 e.printStackTrace();
          	return;
        }
   }
    
    public int nextId() {
      
            ResultSet rs = null;
            int id = 0;
            try {
            String query = "SELECT max(order_id) nextId from BGDR0015.ORDERS";
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                id = rs.getInt("nextId");
            }
                
            } catch (SQLException ex) {
                Logger.getLogger(OrderServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            return id + 1;
    }
    
        public int orderItemsId() {
      
            ResultSet rs = null;
            int id = 0;
            try {
            String query = "SELECT max(orderItemsId) nextId from BGDR0015.ORDERITEMS";
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                id = rs.getInt("nextId");
            }
                
            } catch (SQLException ex) {
                Logger.getLogger(OrderServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            return id + 1;
    }
    // Input items
    public void putItems(HttpServletRequest request) {
        String id = (String)request.getSession().getAttribute("userid");
        int userid = Integer.parseInt(id);
        double totalPrice = Double.parseDouble(request.getParameter("totalPrice"));
     
        // Open Database Connection
        try {
            // load the driver
            //Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            Class.forName("oracle.jdbc.OracleDriver").newInstance();
            //Get a Connection to the database          		
            connection = DriverManager.getConnection("jdbc:oracle:thin:@dilbert.humber.ca:1521:grok ", "YOURUSERNAME", "PASSWORD");

            System.out.println("Connection successful!");
        }  catch( Exception e ) { 
        // problem loading driver, class not exist?
            e.printStackTrace();
        }
        
        // Add the total price
        try {
            Statement insertStatement = connection.createStatement();
            int orderid = nextId();
            String query = "insert into orders values("+orderid+","+userid+","+totalPrice+")";
            insertStatement.executeUpdate(query);
            
            // Record all items in the cart as well
            for (Map.Entry<Integer, Item> entry : shoppingCart.entrySet()) {
                String itemName = entry.getValue().getName();
                int nextOrderItemsId = orderItemsId();
                int item_id = entry.getValue().getId();
                int item_quantity = entry.getValue().getQuantity();
                // Check if it matches the name, then set the quantity.
                //entry.getValue().setQuantity(quantity); // Set the quantity of the item.
                 query = "INSERT INTO ORDERITEMS VALUES("+nextOrderItemsId+", "+orderid+", "+item_id+", "+item_quantity+")";
                 insertStatement.executeUpdate(query);
            }
            
           
        } catch (SQLException ex) {
            Logger.getLogger(OrderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Get items
    public ArrayList<Item> getItems() {
        
        ResultSet rs = null;
            try {
            String query = "SELECT * from BGDR0015.ITEMS";
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                String name = rs.getString("NAME");
                String price = rs.getString("PRICE");
                String itemId = rs.getString("ITEM_ID");
                Item item = new Item(name, Double.parseDouble(price), Integer.parseInt(itemId));
                items.add(item);
            }
                return items;
            } catch (SQLException ex) {
                Logger.getLogger(OrderServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        return null;
    }

}
