/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Bean;

import java.text.DecimalFormat;

/**
 *
 * @author aeRO Gaming
 */
public class Item {
    
    public String name;
    public double price;
    public int id;
    private int quantity = 1; // Default quantity is 1
    
    // Constructor
    public Item(String name, double price, int id) {
        this.name = name;
        this.price = price;
        this.id = id;
    }
    
    public Item(int id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }
    
    public double twoDecimal(double number){
        DecimalFormat f = new DecimalFormat("##.00");
        return Double.parseDouble(f.format(number));
    }
    
    public double getTotalPrice() {

        DecimalFormat f = new DecimalFormat("##.00");
        
        if(quantity == 0){
            return Double.parseDouble(f.format(price));
        }else {
           return Double.parseDouble(f.format(price * quantity));
        }
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
}
