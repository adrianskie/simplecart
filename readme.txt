--Date: 20/02/14
--Author: Adrian Roy A. Baguio
--Description: This is a simple cart system written in jsp.
It uses session and objects in OOP architecture. Also, JSP and
servlets are separated. Servlets here acts like a controller
and JSP are used to display markup language.

Here you will learn:
- How to make objects items using java.
- How to use session for storing userID.
- How to manipulate items inside a cart Object(I created a hash map).
- How to insert and fetch information in Oracle Database
- How to manipulate data from Servlets to JSP
- How to make aliases web address using web.xml

If you have any questions, contact email me at
bgdr0015@humbermail.ca or visit my portfolio 
at www.aerogaming.org/adrian

Thank you!